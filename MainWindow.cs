﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace semechki
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        const int CollumnCount = 9;
        const int FirstCounOfNumbers = 27;

        List<Number> Numbers = new List<Number>();
        Choice MyChoice = new Choice();
        List<Element> Elements = new List<Element>();
        int ft = -1;
        int st = -1;
        private System.Drawing.Drawing2D.GraphicsPath mousePath = new System.Drawing.Drawing2D.GraphicsPath();

        class Element
        {
            public Number Number { set; get; }
            public int Collumn { set; get; }
            public int Row { set; get; }
            private bool IsActive;

            public Element(Element el)
            {
                Number = el.Number;
                IsActive = el.GetActivationState();
                Row = 0;
                Collumn = 0;
            }
            public Element()
            {
                Number = null;
                IsActive = true;
                Row = 0;
                Collumn = 0;
            }
            public void Deactivate()
            {
                IsActive = false;
            }

            public void Activate()
            {
                IsActive = true;
            }

            public bool GetActivationState()
            {
                return IsActive;
            }
        }

        class Choice
        {

            public PictureBox FirstPB { set; get; }
            public PictureBox SecondPB { set; get; }
            public int FirstSelection { set; get; }
            public int SecondSelection { set; get; }


            public Choice()
            {
                
                FirstPB = null;
                SecondPB = null;
                FirstSelection = 0;
                SecondSelection = 0;
            }

            public void DisposeFirst()
            {
                FirstPB = null;
            }
            public void DisposeSecond()
            {
                SecondPB = null;
            }

            public void Dispose()
            {

                FirstPB = null;
                SecondPB = null;
            }
        }

        class Number
        {
            public int Value {set;get;}
            public Bitmap Image {set;get;}
            public Bitmap SelectedImage { set; get; }
            public Bitmap DeletedImage { set; get; }

            public Number(int val, Bitmap img, Bitmap simg, Bitmap dimg)
            {
                Value = val;
                Image = img;
                SelectedImage = simg;
                DeletedImage = dimg;
            }
        }

        public void NewGame(object sender, EventArgs e)
        {
            Elements.Clear();
            tableLayoutPanel1.Controls.Clear();
            panel1.Refresh();
            button1.Enabled = true;


            Bitmap[] Images = new Bitmap[9];
            Bitmap[] SImages = new Bitmap[9];
            Bitmap[] DImages = new Bitmap[9];
            LoadImages(Images, SImages,DImages);
            

            for (int i = 0; i < 9; i++)
            {
                Number n = new Number(i + 1, Images[i],SImages[i],DImages[i]);
                Numbers.Add(n);

            }

            GenerateLevel();
            

        }

        public void LoadImages(Bitmap[] img, Bitmap[] simg, Bitmap[] dimg)
        {
            string[] path = Directory.GetFiles(Application.StartupPath + "/images/", "*.png");
            
            int i = 0;
            int j = 0;
            int k = 0;
            int p = 0;
            foreach (string s in path)
            {
                Bitmap im = new Bitmap(s);
                if (p < 9)
                {
                    img[i] = im;
                    i++;
                }
                else if (p < 18)
                {
                    simg[j] = im;
                    j++;
                }
                else
                {
                    dimg[k] = im;
                    k++;
                }
                p++;
            }
                    
        }

        public void GenerateLevel()
        {
            List<Number> temp = new List<Number>();


            int k = 0;
            for (int i = 0; i < FirstCounOfNumbers ; i++)
            {            
                PictureBox pc = new PictureBox();
                pc.Click += new EventHandler(this.SelectElement);
                pc.Size = new Size(50,50);
                pc.SizeMode = PictureBoxSizeMode.CenterImage;
                if (i < 9)
                {
                    pc.Image = Numbers[i].Image;
                    //pc.Tag = Numbers[i].Value;
                    temp.Add(Numbers[i]);
                }
                else if ( i % 2 == 1)
                {
                    pc.Image = Numbers[0].Image;
                    //pc.Tag = Numbers[0].Value;
                    temp.Add(Numbers[0]);

                }
                else
                {
                    pc.Image = Numbers[k].Image;
                    //pc.Tag = Numbers[k].Value;
                    temp.Add(Numbers[k]);
                    k++;
                }
                pc.Tag = i.ToString();
                tableLayoutPanel1.Controls.Add(pc);
                
                

            }
            int p = 0;
            foreach (Control pb in this.tableLayoutPanel1.Controls)
            {
                Element el = new Element();
                el.Activate();
                el.Number = temp[p];
                TableLayoutPanelCellPosition cp = tableLayoutPanel1.GetPositionFromControl(pb);
                el.Row = cp.Row;
                el.Collumn = cp.Column;
                Elements.Add(el); 

                p++;
            }
            
        }

        private void SelectElement(object sender, EventArgs e)
        {

            if (MyChoice.FirstPB == null)
            {
                MyChoice.FirstPB = (sender) as PictureBox;
                ft = Convert.ToInt32(MyChoice.FirstPB.Tag);
                MyChoice.FirstSelection = Elements[ft].Number.Value;
                listBox1.Items.Add("FT = " + ft.ToString());
                if (Elements[ft].GetActivationState())
                {
                    MyChoice.FirstPB.Image = Elements[ft].Number.SelectedImage;
                }
                else
                {
                    MyChoice.FirstPB.Image = Elements[ft].Number.Image;
                    MyChoice.DisposeFirst();
                }
            }
            else if (MyChoice.SecondPB == null)
            {
                
                MyChoice.SecondPB = (sender) as PictureBox;
                st = Convert.ToInt32(MyChoice.SecondPB.Tag);
                MyChoice.SecondSelection = Elements[st].Number.Value;
                listBox1.Items.Add("st = " + st.ToString());
                if (MyChoice.FirstPB == MyChoice.SecondPB)
                {
                    MyChoice.FirstPB.Image = Elements[ft].Number.Image;
                    MyChoice.SecondPB.Image = Elements[st].Number.Image;
                    MyChoice.Dispose();
                }
                else
                {
                    if (IsCorrect())
                    {
                        MyChoice.FirstPB.Image = Elements[ft].Number.DeletedImage;
                        MyChoice.SecondPB.Image = Elements[st].Number.DeletedImage;
                        Elements[ft].Deactivate();
                        Elements[st].Deactivate();
                        tableLayoutPanel1.Refresh();
                        //проверить,может нужно удалить строку)) 
                        RowIsFull(Elements[ft].Row, Elements[st].Row);
                        if (IsGameOver())
                        {
                            Elements = null;
                            tableLayoutPanel1.Controls.Clear();
                            button1.Enabled = false;
                            MessageBox.Show("Game over");
                        }

                    }
                    else
                    {
                        MyChoice.FirstPB.Image = Elements[ft].Number.Image;
                        MyChoice.SecondPB.Image = Elements[st].Number.Image;
                    }

                    MyChoice.Dispose();

                }

            }
        }

        public bool IsCorrect()
        {
            bool isCorrect = false;
            listBox1.Items.Clear();
            if (MyChoice.FirstSelection + MyChoice.SecondSelection == 10 || MyChoice.FirstSelection == MyChoice.SecondSelection)
            {
                listBox1.Items.Add(" == 10 или одинаковые");
                if (Elements[ft].Row == Elements[st].Row || Elements[ft].Collumn == Elements[st].Collumn)
                {
                    listBox1.Items.Add("в одной строке или столбце");
                    int p = Math.Abs(Elements[ft].Collumn - Elements[st].Collumn);
                    int t = Math.Abs(Elements[ft].Row - Elements[st].Row);
                    if (p == 1 || t == 1)
                    {
                        listBox1.Items.Add("рядышком");
                        isCorrect = true;
                    }
                    else if (!FindActive(ft, st))
                    {
                        listBox1.Items.Add("активных между выделенными не найдено");
                        isCorrect = true;
                    }
                }
                else
                {
                    listBox1.Items.Add("В разных строках");


                    if (FindActive(ft, st) == false)
                    {
                        listBox1.Items.Add("активных между выделенными не найдено");
                        isCorrect = true;
                    }
                    else
                    {
                        listBox1.Items.Add("найдены активные");
                        isCorrect = false;
                    }
                }

            }

            return isCorrect;
            //return true;
        }

        public void RowIsFull(int first, int second)
        {
            bool isFullF = true;
            bool isFullS = true;
            
            if (first == second)
            {
                foreach (Element el in Elements)
                {
                    if (el.Row == first && el.GetActivationState())
                    {
                        isFullF = false;
                    }
                }
            }
            else
            {
                foreach (Element el in Elements)
                {
                    if (el.Row == first && el.GetActivationState())
                    {
                        isFullF = false;
                    }
                    if (el.Row == second && el.GetActivationState())
                    {
                        isFullS = false;
                    }
                }
            }
            if (first == second)
            {
                //tableLayoutPanel1.Visible = false;
                if (isFullF)
                {
                    DeleteRow(first);
                }
                //tableLayoutPanel1.Visible = true;
            }
            else
            {
                //tableLayoutPanel1.Visible = false;
                if (isFullF)
                {
                    DeleteRow(first);
                }
                if (isFullS)
                {
                    DeleteRow(second);
                }
                //tableLayoutPanel1.Visible = true;
            }
        }

        public void DeleteRow(int numb)
        {

            foreach (PictureBox pb in tableLayoutPanel1.Controls)
            {
                if (Elements[Convert.ToInt32(pb.Tag)].Row == numb)
                {
                    pb.Visible = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool FindActive(int i, int j)
        {
            bool search = false;

            if (i < j)
                {
                    i++;
                    while (i < j)
                    {
                        if (Elements[i].GetActivationState())
                        {
                            search = true;
                        }
                        i++;
                    }
                }

                else
                {
                    j++;
                    while (j < i)
                    {
                        if (Elements[j].GetActivationState())
                        {
                            search = true;
                        }
                        j++;
                    }
                }
            
            if (search == false)
            {
                listBox1.Items.Add("не найдено в строке");
                    int p = Elements[ft].Collumn;

                    if (i < j)
                    {
                        for (int k = 0; k < Elements.Count; k++)
                        {
                            if ( k > i && k < j&& Elements[k].Collumn == p && Elements[k].GetActivationState())
                            {
                                search = true;
                            }
                        }
                    }

                    else
                    {
                        for (int k = 0; k < Elements.Count; k++)
                        {
                            if (k < i && k > j && Elements[k].Collumn == p && Elements[k].GetActivationState())
                            {
                                search = true;
                            }
                        }
                    }
            }

            return search;
        }

        private void AddNewElements(object sender, EventArgs e)
        {
            int c = Elements.Count;
            for (int i = 0; i < c; i++)
            {
                if (Elements[i].GetActivationState())
                {
                    //temp.Add(Elements[i]);
                    Element el = new Element(Elements[i]);
                    Elements.Add(el);
                }
                else
                {
                    listBox1.Items.Add(i);
                }
            }

            //for (int i = 0; i < temp.Count; i++)
            //{
            //    Elements.Add(temp[i]);
            //}
            //temp.Clear();

            
            tableLayoutPanel1.Controls.Clear();

            for (int i = 0; i < Elements.Count; i++)
            {
                PictureBox pc = new PictureBox();
                pc.Click += new EventHandler(this.SelectElement);
                pc.Size = new Size(50, 50);
                pc.SizeMode = PictureBoxSizeMode.CenterImage;
                if (Elements[i].GetActivationState())
                {
                    pc.Image = Elements[i].Number.Image;
                }
                else
                {
                    pc.Image = Elements[i].Number.DeletedImage;
                }
                pc.Tag = i.ToString();
                tableLayoutPanel1.Controls.Add(pc);
            }
          
            for (int i = 0; i < tableLayoutPanel1.Controls.Count; i++)
            {
                TableLayoutPanelCellPosition cp = tableLayoutPanel1.GetPositionFromControl(tableLayoutPanel1.Controls[i]);
                Elements[i].Row = cp.Row;
                Elements[i].Collumn = cp.Column;
            }
            
        }

        public bool IsGameOver()
        {
            bool isOver = true;

            foreach (Element el in Elements)
            {
                if (el.GetActivationState())
                {
                    isOver = false;
                }
            }
            return isOver;
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SettingsWindow s = new SettingsWindow();
            s.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Rules r = new Rules();
            r.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.Show();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            panel1.AutoScroll = true;
        }

        void MainWindow_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            try
            {
                if (e.Delta < 0)
                {
                    panel1.VerticalScroll.Value+=20;
                }
                else
                {
                    panel1.VerticalScroll.Value-=20;
                }
            }
            catch (Exception) { };
        }
    }
}
